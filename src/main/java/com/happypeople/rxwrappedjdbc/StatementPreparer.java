package com.happypeople.rxwrappedjdbc;

import java.sql.PreparedStatement;
import java.sql.SQLException;

interface StatementPreparer {
	void accept(PreparedStatement stat) throws SQLException;
}