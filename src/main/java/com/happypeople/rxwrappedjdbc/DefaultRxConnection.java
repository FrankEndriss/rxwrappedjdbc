package com.happypeople.rxwrappedjdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;

/**
 * Basic implementation of Connection / PreparedStatement wrapped by Rx-ified
 * api.
 *
 * The idea is to execute all blocking operations in a runtime configurable
 * Scheduler while offering a non-blocking api.
 */
public class DefaultRxConnection implements RxConnection {
	private final Connection jdbcConnection;

	/**
	 * @param jdbcConnection The delegate.
	 */
	public DefaultRxConnection(@NonNull final Connection jdbcConnection) {
		this.jdbcConnection = jdbcConnection;
	}

	@Override
	public Observable<RxPreparedStatement> prepareStatement(final String sql) {
		return Observable.defer(() -> {
			return (observer) -> {
				try {
					observer.onNext(new RxPreparedStatementImpl(jdbcConnection.prepareStatement(sql)));
					observer.onComplete();
				} catch (final Throwable t) {
					observer.onError(t);
				}
			};
		});
	}

	private static class RxPreparedStatementImpl implements RxPreparedStatement {
		private final PreparedStatement ps;

		RxPreparedStatementImpl(final PreparedStatement ps) {
			this.ps = ps;
		}

		@Override
		public <T> Observable<T> executeQuery(final StatementPreparer preparer, final RowMapper<T> rowMapper) {
			final ObservableSource<T> source = new ObservableSource<T>() {
				@Override
				public void subscribe(final Observer<? super T> observer) {
					try {
						if (preparer != null)
							preparer.accept(ps);

						final ResultSet rs = ps.executeQuery();
						while (rs.next())
							observer.onNext(rowMapper.apply(rs));
					} catch (final Throwable e) {
						observer.onError(e);
					}
					observer.onComplete();
				}
			};

			return Observable.defer(() -> source);
		}

		@Override
		public Observable<Long> executeUpdate(final StatementPreparer preparer) {
			final ObservableSource<Long> source = new ObservableSource<Long>() {
				@Override
				public void subscribe(final Observer<? super Long> observer) {
					try {
						if (preparer != null)
							preparer.accept(ps);

						observer.onNext(Long.valueOf(ps.executeUpdate()));
					} catch (final Throwable e) {
						observer.onError(e);
					}
					observer.onComplete();
				}
			};

			return Observable.defer(() -> source);
		}
	}
}
