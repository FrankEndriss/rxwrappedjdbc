package com.happypeople.rxwrappedjdbc;

import io.reactivex.Observable;

interface RxPreparedStatement {
	/**
	 * Prepares and executes the query on every subscription, sends the resultSet
	 * through the rowMapper, and delivers the mapped objects T to the subscribed
	 * Observer.
	 *
	 * Use Observable.subscribeOn(Scheduler) to specify in which Scheduler the
	 * blocking calls are executed. This is
	 * 
	 * <pre>
	 * - the call to the preparer
	 * - the call to the underlying jdbc PreparedStatement.execute()
	 * - the iteration over the ResultSet
	 * - calls to your rowMapper
	 * </pre>
	 *
	 * Use Observable.observeOn(Scheduler) to specify on which threads your
	 * callbacks (i.e. onNext(T)) will be called.
	 *
	 * @return the prepared statement wrapped into an Observable which executes it
	 *         on subscribe(...)
	 */
	public <T> Observable<T> executeQuery(final StatementPreparer preparer, final RowMapper<T> rowMapper);

	/**
	 * Does the same as executeQuery, but with an update, and hence without an
	 * result set. So, one needs no RowMapper.
	 *
	 * @param preparer StatementPreparer for this execution of the statement.
	 * @return Observable which executes the update per subscription, delivering the
	 *         jdbc usual update count.
	 */
	public Observable<Long> executeUpdate(final StatementPreparer preparer);
}