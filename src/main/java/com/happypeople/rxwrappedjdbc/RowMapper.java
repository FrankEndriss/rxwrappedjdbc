package com.happypeople.rxwrappedjdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

interface RowMapper<T> {
	T apply(ResultSet resultSet) throws SQLException;
}