package com.happypeople.rxwrappedjdbc;

import java.sql.ResultSet;

import io.reactivex.Observable;

public interface RxConnection {
	/**
	 * RowMapper you can use if you want no mapping, i.e. you want to act on the
	 * ResultSet for yourself.
	 */
	RowMapper<ResultSet> NO_MAPPING = (rs) -> rs;
	/**
	 * Preparer for Statements which do not have to be prepared, it does nothing.
	 */
	StatementPreparer NO_PREPARE = null;

	/**
	 * Prepare a statement from a sql string. Every call to the returned
	 * Observable.subscribe(...) will create a new RxPreparedStatement. The possibly
	 * blocking subscription is executed on the specified Scheduler. Use
	 * Observable.subscribeOn(Scheduler) to explicitly specify one.
	 *
	 * @param sql The sql statement to prepare.
	 * @return The prepared statement Observable.
	 */
	Observable<RxPreparedStatement> prepareStatement(final String sql);
}