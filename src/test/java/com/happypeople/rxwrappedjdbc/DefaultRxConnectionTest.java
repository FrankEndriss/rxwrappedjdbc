package com.happypeople.rxwrappedjdbc;

import static org.junit.jupiter.api.Assertions.fail;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

class DefaultRxConnectionTest {
	private Connection jdbcConnection;
	private DefaultRxConnection sut;
	private boolean hot = false;

	private final Answer<?> defaultAnswer = invocation -> {
		if (hot) {
			final Method method = invocation.getMethod();
			throw new RuntimeException(
					"not implemented: " + method.getDeclaringClass().getSimpleName() + "::" + method.getName());
		}
		return null;
	};

	private <T> T mock(Class<T> clazz) {
		return Mockito.mock(clazz, Mockito.withSettings().defaultAnswer(defaultAnswer));
	}

	@BeforeEach
	void setUp() {
		jdbcConnection = mock(Connection.class);
		sut = new DefaultRxConnection(jdbcConnection);
	}

	@Test
	void test() throws Throwable {
		hot = false;
		final CountDownLatch latch = new CountDownLatch(1);
		final Throwable[] errors = new Throwable[1];

		final PreparedStatement preparedStatementMock = mock(PreparedStatement.class);
		Mockito.when(jdbcConnection.prepareStatement(Mockito.anyString())).thenReturn(preparedStatementMock);

		hot = true;
		sut.prepareStatement("SELECT * FROM mytable WHERE id = ?").subscribe((preparedStatement) -> {
			preparedStatement.executeQuery(stat -> {
				stat.setString(1, "myId");
			}, (resultSet) -> {
				return resultSet.getString(1);
			});
		}, (t) -> errors[0] = t, () -> latch.countDown());

		try {
			latch.await(1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			fail("interrupted");
		}

		if (errors[0] != null)
			throw errors[0];
	}
}
