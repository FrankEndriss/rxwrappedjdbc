package com.happypeople.rxwrappedjdbc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.schedulers.Schedulers;

/**
 * Utility to test/demonstrate features of RxJava
 *
 */
public class RxJavaTest {
	private static ExecutorService executorFixed;
	private static ExecutorService executorSingle;

	@BeforeAll
	static void init() {
		executorFixed = Executors.newFixedThreadPool(8);
		executorSingle = Executors.newSingleThreadExecutor();
	}

	private final DateFormat dateFormat = new SimpleDateFormat("HH.mm.ss.SSSSSS");

	void log(final String msg) {
		System.out.println(Thread.currentThread().getName() + " " + dateFormat.format(new Date()) + " " + msg);
	}

	@Test
	void testDefer1() throws InterruptedException {
		final CountDownLatch latch = new CountDownLatch(1);
		log("testDefer1");
		final ObservableSource<Integer> source = new ObservableSource<Integer>() {
			@Override
			public void subscribe(final Observer<? super Integer> observer) {
				log("submitting");
				executorFixed.submit(() -> {
					log("executing in executor");
					observer.onNext(Integer.valueOf(1));
					observer.onComplete();
					latch.countDown();
					log("executing in executor finished");
				});
				log("submitted");
			}
		};

		log("creating Observable");
		final Observable<Integer> observable = Observable.defer(() -> source);
		log("created Observable");

		observable.subscribe((i) -> System.out.println("onNext(): " + i));
		log("subscribed");

		latch.await();
		log("finished");
	}

	@Test
	void testDefer2() throws InterruptedException {
		log("testDefer2");
		final ObservableSource<Integer> source = new ObservableSource<Integer>() {
			@Override
			public void subscribe(final Observer<? super Integer> observer) {
				log("submitting");
				observer.onNext(Integer.valueOf(1));
				observer.onComplete();
				log("subscribe finished");
			}
		};

		log("creating Observable");
		final Observable<Integer> observable = Observable.defer(() -> source);
		log("created Observable");

		// use two different pre-defined schedulers, just to see if two different ones
		// are used
		observable.subscribeOn(Schedulers.computation()).observeOn(Schedulers.io())
				.subscribe((i) -> log("onNext(): " + i), (t) -> log("onError"), () -> log("onComplete"));
		log("subscribed1");

		log("finished");
		Thread.sleep(1000);
	}

}
