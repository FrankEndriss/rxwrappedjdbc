# rxWrappedJdbc

This project implement a Jdbc connction wrapper
with an reactive api.

A Factory is able to create these "reactive connections" from 
a standard jdbc connection and an Excecutor.

Of course, the RxConnection interface is reduced in comparison
with the java.sql.Connection.

